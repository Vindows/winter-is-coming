import React from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import rootReducer from './reducers'
import thunkMiddleware from 'redux-thunk';

/**
 * Makes it easier to write tests for components connected to the redux store. 
 */
export default ({children, initialState = {}}) => {
    const store = createStore(rootReducer, initialState, applyMiddleware(thunkMiddleware));
    return (
        <Provider store={store}>
            {children}
        </Provider>
    )
}