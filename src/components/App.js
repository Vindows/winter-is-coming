import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchSkills } from "./../actions";
import PropTypes from 'prop-types';
import CardList from './CardList';
import './../stylesheets/App.scss';
import AddSkill from './AddSkill';

/**
 * Container component fetches skills and renders cards
 */
class App extends Component {
    componentDidMount() {
        this.props.onFetchSkills();
    }
    render() {
        return (
            <div className="app">
                <h1 className="primary-heading">add skills to list</h1>
                <AddSkill />
                <CardList skills={this.props.skills} isPending={this.props.isPending} />
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        isPending: state.skillsReducer.isPending,
        skills: state.skillsReducer.skills,
        error: state.skillsReducer.error
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        onFetchSkills: () => dispatch(fetchSkills())
    };
}

CardList.propTypes = {
    skills: PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string,
      experience: PropTypes.string
    }),
    isPending: PropTypes.bool,
    error: PropTypes.object
  }

export default connect(mapStateToProps, mapDispatchToProps)(App);