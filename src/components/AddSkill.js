import React, { Component } from "react";
import { connect } from "react-redux";
import { addSkill } from "./../actions";
import InputSkill from "./InputSkill";
import InputExperience from "./InputExperience";
import "./../stylesheets/AddSkill.scss";

/**
 * Uses local state to store properties received from InputExperience and InputSkill component.
 * 
 */
class AddSkill extends Component {
  constructor() {
    super();
    this.state = {
      id: "",
      name: "",
      experience: "",
      errors: {}
    };
  }
  onSkillChange(value) {
    this.setState({
      name: value
    });
  }
  onExpChange(value) {
    this.setState({
      experience: value
    });
  }
  isInputValid(formValues) {
    let errors = {};
    if (formValues.name.length < 4 || formValues.name.length > 255) {
      errors.name = "The length of skill cannot be smaller than 4 characters or longer than 255 characters.";
    }
    if (!formValues.experience) {
      errors.experience = "Plese select your level of experience";
    }

    // Checks to see if object is empty
    if (Object.keys(errors).length === 0 && errors.constructor === Object) { 
      this.setState({ errors: errors })
      return true;
    } else {
      this.setState({
        errors: errors
      })
      return false;
    }
  }
  renderErrors() {
    let errors = [];
    for (let key in this.state.errors) {
      errors.push(<li key={key}>{this.state.errors[key]}</li>)
    }
    return errors;
  }
  onSubmit(event) {
    event.preventDefault();
    if (this.isInputValid(this.state)) {
      this.props.onSubmit(this.state);
      this.setState({
        id: "",
        name: "",
        experience: ""
      });
    }
  }
  render() {
    return (
      <div className="tc">
        <form onSubmit={this.onSubmit.bind(this)}>
          <div className="input-skill">
            <InputSkill
              skill={this.state.name}
              onSkillChange={this.onSkillChange.bind(this)}
            />
          </div>
          <div className="input-experience">
            <InputExperience
              experience={this.state.experience}
              onExpChange={this.onExpChange.bind(this)}
            />
            <button
              disabled={
                !this.state.name || !this.state.experience ? true : false
              }
              type="submit"
              className="btn-add"
            >
              add skills
            </button>
          </div>
        </form>
        <ul className="error">{this.renderErrors()}</ul>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onSubmit: skill => dispatch(addSkill(skill))
  };
};

const mapStateToProps = (state) => {
  return {
    isPending: state.skillsReducer.isPending,
    skills: state.skillsReducer.skills,
    error: state.skillsReducer.error
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AddSkill);
