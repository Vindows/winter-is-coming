import React, { Component } from "react";
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { deleteSkill } from './../actions/index';
import "./../stylesheets/Card.scss";


/**
 * Core component of the application, displays skill and experience
 */
class Card extends Component {
  deleteSkill() {
    this.props.deleteSkill(this.props.id)
  }
  render() {
    return (
      <div className="card">
        <div className={this.props.index < 5 ? "card-id black-id" : "card-id"}>
          {this.props.id}
        </div>
        <div className="card-content">
          <span className="content-skill">{this.props.skill}</span>
          <span className="content-experience">{this.props.experience}</span>
        </div>
        <div className="card-delete" onClick={this.deleteSkill.bind(this)}>
          X
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    deleteSkill: id => dispatch(deleteSkill(id))
  }
}

const mapStateToProps = (state) => {
  return {
    isPending: state.skillsReducer.isPending,
    skills: state.skillsReducer.skills,
    error: state.skillsReducer.error
  }
}

Card.propTypes = {
  id: PropTypes.number,
  index: PropTypes.number,
  skill: PropTypes.string,
  experience: PropTypes.string
}

export default connect(mapStateToProps, mapDispatchToProps)(Card);
