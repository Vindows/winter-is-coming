import React from 'react';
import { shallow } from 'enzyme';
import Root from './../../Root';
import App from '../App';
import CardList from '../CardList';
import AddSkill from '../AddSkill';

describe('App', () => {
  let wrapper;
  beforeEach(() => {
    const initialState = {
      skillsReducer: {skills: [{id: '1', name: 'React', experience: '< 1 year'}, {id: '2', name: 'Angular', experience: '1 - 3 years'}]}
    }
    wrapper = mount(
      <Root initialState={initialState}>
        <App />
      </Root>
    );
  })
    it('should render a <div />', () => {
      expect(wrapper.find('div').length).toBeGreaterThan(0);
    });

    it('should render the CardList Component', () => {
      expect(wrapper.find(CardList).length).toEqual(1);
    });

    it('should render AddSkill component', () => {
      expect(wrapper.find(AddSkill).length).toEqual(1);
    });

});