import React from "react";
import { shallow } from "enzyme";
import InputExperience from "../InputExperience";

describe("InputExperience", () => {
  let wrapper;
  beforeEach(() => {
    wrapper = shallow(<InputExperience />);
  });

  it("has a select element", () => {
    expect(wrapper.find("select").length).toEqual(1);
  });
  
});
