import React from 'react';
import { mount } from 'enzyme';
import Card from '../Card';
import Root from './../../Root';

describe('Card', () => {
    let wrapper;
    beforeEach(() => wrapper = mount(
    <Root>
      <Card key={1}
            index={1}
            id={123}
            skill='New skill'
            experience='1 - 3 years' />
    </Root>));

    afterEach(() => {
      wrapper.unmount();
    })
  
    it('should render three <div /> elemets', () => {
      expect(wrapper.find('div').length).toEqual(4);
    });

    it('should render three <span /> elements', () => {
      expect(wrapper.find('span').length).toEqual(2);
    });

    it('should receive props and render', () => {
      expect(wrapper.find('.content-skill').text()).toEqual('New skill');
      expect(wrapper.find('.content-experience').text()).toEqual('1 - 3 years');
      expect(wrapper.find('.card-id').text()).toEqual('123');
    });
});