import React from 'react';
import { shallow } from 'enzyme';
import CardList from './../CardList';
import Root from './../../Root';



describe('CardList', () => {
  let wrapper;
  beforeEach(() => {
    const props = {
      isPending: false,
      skills: [{ id: '1', name: 'React', experience: '< 1 year' }, { id: '2', name: 'Angular', experience: '1 - 3 years' }]
    }

    wrapper = mount(
      <Root><CardList skills={props.skills} isPending={props.false} /></Root>
    );
  });

  it('should render a <div />', () => {
    expect(wrapper.find('div').length).toEqual(9);
  });

  it('should create one Card element per skill', () => {
    expect(wrapper.find('Card').length).toEqual(2);
  })
});