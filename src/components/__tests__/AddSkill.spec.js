import AddSkill from "./../AddSkill";
import InputSkill from "./../InputSkill";
import InputExperience from "./../InputExperience";
import Root from './../../Root'; 

describe("AddSkill", () => {
  let wrapper;
  beforeEach(() => { 
    const initialState = {
      skillsReducer: {skills: [{id: '1', name: 'React', experience: '< 1 year'}, {id: '2', name: 'Angular', experience: '1 - 3 years'}]}
    }
    wrapper = mount(<Root><AddSkill /></Root>)
  });
  afterEach(() => {
    wrapper.unmount();
  });

  it("should render three <div />", () => {
    expect(wrapper.find("div").length).toEqual(3);
  });

  it("should render an <ul />", () => {
    expect(wrapper.find("ul").length).toEqual(1);
  });

  it("should render the InputSkill Component", () => {
    expect(wrapper.find(InputSkill).length).toEqual(1);
  });

  it("should render the InputExperience Component", () => {
    expect(wrapper.find(InputExperience).length).toEqual(1);
  });
});

describe("AddSkill inputs", () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount(<Root><AddSkill /></Root>);
    wrapper.find("input").simulate("change", {
      target: { value: "Skill Name" }
    });
    wrapper.find("select").simulate("change", {
      target: { value: "< 1 year"}
    })
    wrapper.update();
  });

  afterEach(() => {
    wrapper.unmount();
  });

  it("has an input that users can type in and select element that user can change", () => {
    expect(wrapper.find("input").prop("value")).toEqual("Skill Name");  
    expect(wrapper.find("select").prop("value")).toEqual("< 1 year");
  });

  it("when form is submitted, text area gets emptied", () => {
    wrapper.find("form").simulate("submit");
    wrapper.update();
    expect(wrapper.find("input").prop("value")).toEqual("");
    expect(wrapper.find("select").prop("value")).toEqual("");
  });
});
