import React from "react";
import { mount } from "enzyme";
import InputSkill from "../InputSkill";

describe("InputSkill", () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount(<InputSkill />);
  });

  it("has an input element", () => {
    expect(wrapper.find("input").length).toEqual(1);
  });
});
