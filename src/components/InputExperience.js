import React, { Component } from 'react';
import PropTypes from 'prop-types';


/**
 * Renders select element, passes experience prop through a callback to AddSkill (Parent) component
 */
class InputExperience extends Component {
    onExpChange(event) {
        this.props.onExpChange(event.target.value);
    }
    render() {
        return(
                <select value={this.props.experience} onChange={this.onExpChange.bind(this)} required>
                    <option value="">Experience</option>
                    <option value="< 1 year">&lt; 1 year</option>
                    <option value="1 - 3 years">1 - 3 years</option>
                    <option value="3 - 5 years">3 - 5 years</option>
                    <option value="5 - 7 years">5 - 7 years</option>
                </select>
        )
    }
}

InputExperience.propTypes = {
    experience: PropTypes.string
}

export default InputExperience;