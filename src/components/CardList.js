import React, { Component } from "react";
import PropTypes from 'prop-types';
import Card from "./Card";
import "./../stylesheets/CardList.scss";

/**
 * Renders list of Card components with array of skills prop.
 */
class CardList extends Component {
  constructor() {
    super();
  }

  renderSkills() {
    if (!this.props.isPending) {
      return this.props.skills.map((skill, index) => {
        return (
          <Card
            key={index}
            index={index}
            id={skill.id}
            skill={skill.name}
            experience={skill.experience}
          />
        );
      });
    } else {
        return "Loading"
    }
  }

  render() {
    return <div className="card-list">{this.renderSkills()}</div>;
  }
}

CardList.propTypes = {
  skills: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
    experience: PropTypes.string
  }))
}

export default CardList;
