import React, { Component } from 'react';
import PropTypes from 'prop-types';

/**
 * Skill name input
 * Passes the value of the input in a callback to AddSkill (Parent) component.
 */
class InputSkill extends Component {
    onSkillChange(event) {
        this.props.onSkillChange(event.target.value);
    }
    render() {
        return (
                <input
                className='searchBox' 
                type='search'
                onChange={this.onSkillChange.bind(this)}
                value={this.props.skill}
                placeholder='Node.js, Postgres, React, etc.' 
                required />
        )
    }
}

InputSkill.propTypes = {
    skill: PropTypes.string
}

export default InputSkill;