import {
  FETCH_SKILLS_FAIL,
  FETCH_SKILLS_PENDING,
  FETCH_SKILLS_SUCCESS,
  ADD_SKILL_SUCCESS,
  ADD_SKILL_PENDING,
  ADD_SKILL_FAIL,
  DELETE_SKILL_PENDING,
  DELETE_SKILL_SUCCESS,
  DELETE_SKILL_FAILL
} from "./../actions/types";

export default function(
  state = { isPending: false, skills: [], error: "" },
  action
) {
  switch (action.type) {
    case ADD_SKILL_SUCCESS:
      state.skills.length === 0 && state.skills
        ? (action.payload.id = 1)
        : (action.payload.id = state.skills[state.skills.length - 1].id + 1);
      return Object.assign({}, state, {
        skills: state.skills.concat(action.payload),
        isPending: false
      });
    case ADD_SKILL_PENDING:
      return Object.assign({}, state, { isPending: true });
    case ADD_SKILL_FAIL:
      return Object.assign({}, state, {
        error: action.payload,
        isPending: false
      });
    case FETCH_SKILLS_PENDING:
      return Object.assign({}, state, { isPending: true });
    case FETCH_SKILLS_SUCCESS:
      return Object.assign({}, state, {
        skills: action.payload,
        isPending: false
      });
    case FETCH_SKILLS_FAIL:
      return Object.assign({}, state, {
        error: action.payload,
        isPending: false
      });
    case DELETE_SKILL_PENDING:
      return Object.assign({}, state, { isPending: true });
    case DELETE_SKILL_SUCCESS:
    console.log("DELETE", action.payload);
      return Object.assign({}, state, {
        skills: state.skills.filter(item => {
          return item.id != action.payload.id
        }),
        isPending: false
      });
    case DELETE_SKILL_FAILL:
      return Object.assign({}, state, {
        error: action.payload,
        isPending: false
      });
    default:
      return state;
  }
}
