import skillsReducer from './../skills';
import { ADD_SKILL_SUCCESS } from './../../actions/types';


it('handles actions of type ADD_SKILL', () => {
    const action = {
        type: ADD_SKILL_SUCCESS,
        payload: { id: '', name: 'New skill', experience: '< 1 year'}
    }

    const newState = skillsReducer({skills: []}, action);
    
    expect(newState).toEqual({isPending: false, skills: [{ id: 1, name: 'New skill', experience: '< 1 year'}]});
});

it('handles action with unknown type', () => {
    const newState = skillsReducer([], {type: 'RANDOM'});

    expect(newState).toEqual([]);
});