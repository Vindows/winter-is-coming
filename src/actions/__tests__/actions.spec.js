import { addSkill } from "../index";
import configureMockStore from "redux-mock-store";
import moxios from "moxios";
import thunk from "redux-thunk";
import * as actions from "./../index";
import * as types from "../types";

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe("async actions", () => {
  beforeEach(() => {
    moxios.install();
  });

  afterEach(() => {
    moxios.uninstall();
  });

  it("creates FETCH_SKILL_SUCCESS when fetching skills has been done", () => {
    moxios.stubRequest("http://localhost:3000/skills?_sort=id", {
      status: 200,
      response: [{ id: 123, name: "fake skill", experience: "< 1 year" }],
      headers: { "content-type": "application/json" }
    });
    const expectedActions = [
      { type: types.FETCH_SKILLS_PENDING },
      {
        type: types.FETCH_SKILLS_SUCCESS,
        payload: [{ id: 123, name: "fake skill", experience: "< 1 year" }]
      }
    ];
    const store = mockStore({ skills: [] });

    return store.dispatch(actions.fetchSkills()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it("creates FETCH_SKILL_FAIL when fetching fails", () => {
    moxios.stubRequest("http://localhost:3000/skills?_sort=id", {
      status: 400,
      response: [{ id: 123, name: "fake skill", experience: "< 1 year" }],
      headers: { "content-type": "application/json" }
    });

    const expectedActions = [
      { type: types.FETCH_SKILLS_PENDING },
      {
        type: types.FETCH_SKILLS_FAIL,
        payload: new Error("Request failed with status code 400")
      }
    ];
    const store = mockStore({ skills: [] });

    return store.dispatch(actions.fetchSkills()).then(() => {
      // return of async actions
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});
