import * as actions from "./types";
  import axios from 'axios';
  
  export const addSkill = (skill) => {
      return function(dispatch) {
          dispatch({type: actions.ADD_SKILL_PENDING})
          axios.post("http://localhost:3000/skills", skill)
          .then(data => dispatch({type: actions.ADD_SKILL_SUCCESS, payload: skill}))
          .catch(error => dispatch({type: actions.ADD_SKILL_FAIL, payload: error}));
      }
  }

  export const deleteSkill = id => {
      console.log(id);
      return function(dispatch) {
          dispatch({ type: actions.DELETE_SKILL_PENDING });
          axios.delete(`http://localhost:3000/skills/${id}`, {data: {id: id}})
          .then(data => dispatch({ type: actions.DELETE_SKILL_SUCCESS, payload: {id: id}}))
          .catch(error => dispatch({ type: actions.DELETE_SKILL_FAILL, payload: error}));
        }
    }
  
  export const fetchSkills = () => dispatch => {
    dispatch({ type: actions.FETCH_SKILLS_PENDING });
    return axios.get("http://localhost:3000/skills?_sort=id")
      .then(response => dispatch({ type: actions.FETCH_SKILLS_SUCCESS, payload: response.data }))
      .catch(error => dispatch({ type: actions.FETCH_SKILLS_FAIL, payload: error }));
  };
  