React App
==========

### Steps to get up and running 

1. Run `npm install`
1. Run `npm run api`
1. In a different terminal 
   1. Run `npm start:dev` to run webpack-dev-server with hot reload 
       OR
   1. Run `npm start:prod` to run the production server  

### Time it took to finish the project
* Approximately 48 hours
* I underestimated roadblocks that rooted out of setting up the environment from scratch
   

### What I would have done if time permitted   
* Planned the app much more before writing any code leading to fewer refactors and more tests early on in the development. 
* Would have written more tests
* Used the full GitFlow workflow