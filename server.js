const express = require('express');
const path = require('path');
const app = express();
const port = process.env.PORT || '8080';

app.use('/dist', express.static('dist'));

app.get('/*', (req, res, next) => {
  const routePath = path.join(__dirname, 'dist/' + 'index.html');
  res.sendFile(routePath);
})


app.listen(port, () => {console.log(`Server is running on port ${port}`)});
